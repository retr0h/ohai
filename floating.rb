#
# Cookbook Name:: ohai
# Plugin:: floating_plugin
#
# Copyright 2012, John Dewey
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

METADATA_ADDR = "169.254.169.254"
METADATA_URL = "/2008-02-01/meta-data"

provides "openstack"

begin
  cmd = "curl http://#{METADATA_ADDR}#{METADATA_URL}/public-ipv4"
  status, stdout, stderr = run_command(:command => cmd)

  openstack Mash.new
  openstack[:public_ipv4] = (stdout.nil? ? "" : stdout)
rescue => e
  Chef::Log.warn "Ohai floating_plugin failed with: '#{e}'"
end

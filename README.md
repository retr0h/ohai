# Ohai Plugins

## Usage

Follow the [Distributing Ohai Plugins](http://wiki.opscode.com/display/chef/Distributing+Ohai+Plugins) directions.

### floating

This plugin discovers a VMs public address in an [OpenStack](http://www.openstack.org/) environment, by making a call to the EC2 meta-data service.

Ohai already has a similar [plugin](https://github.com/opscode/ohai/blob/master/lib/ohai/plugins/eucalyptus.rb#L41), but makes assumptions on your VM's mac address.

From cookbooks:

    node[:openstack][:public_ipaddress]
      or
    node.openstack.public_ipaddress

From knife search:

    $ knife search node 'name:server-67.novalocal' -a openstack.public_ipv4
    1 items found

    id:                     server-67.novalocal
    openstack.public_ipv4:  75.x.x.x

### ipmi

This plugin collects a metal system’s IPMI address. Debian systems require the ‘ipmitool’ package and the following modules loaded.

    if ! grep ipmi /etc/modules > /dev/null; then
        echo ipmi_si >> /etc/modules
        echo ipmi_devintf >> /etc/modules
    fi

From cookbooks:

    node[:ipmi][:address]
    node[:ipmi][:mac_address]
      or
    node.ipmi.address
    node.ipmi.mac_address

From knife search:

    $ knife search node 'name:o4r1*' -a ipmi
    1 items found

    id:    o4r1.example.com
    ipmi:
      address:      172.16.5.13
        mac_address:  e8:9a:8f:a0:32:57

### lldp

This plugin collects a metal system’s [LLDP](http://en.wikipedia.org/wiki/Link_Layer_Discovery_Protocol) information. Debian systems require the ‘lldpd’ package.

For further information see a short [post](http://dewey.ws/blog/2012/03/05/lldp-discovery-with-chef-and-ohai/) about this plugin.

From knife search:

    $ knife search node "name:o5r1*" -a lldp.eth2
    1 items found

    id:         o5r1
    lldp.eth2:
      age:      3 days, 14:38:13
      chassis:
        Bridge:
          enabled:  on
        Router:
          enabled:  off
        descr:    Arista Networks EOS version 4.8.3 running on an Arista Networks DCS-7124SX
        mac:      aa:bb:cc:dd:ee:ff
        mgmt-ip:  192.168.0.108
        name:     xx-R1
      port:
        aggregation:  1000052
        descr:        Not received
        ifname:       Ethernet2
        mfs:          9236
      rid:      4
      via:      LLDP
      vlan:
        pvid:     yes
        vlan-id:  1

## License

Author:: John Dewey (<john@dewey.ws>)

Copyright 2012, John Dewey

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

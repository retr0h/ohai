#
# Cookbook Name:: ohai
# Plugin:: llpd
#
# Copyright 2012, John Dewey
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

provides "linux/llpd"

lldp Mash.new

def hashify h, list
  if list.size == 1
    return list.shift
  end 

  key    = list.shift
  h[key] ||= {}
  h[key] = hashify h[key], list
  h
end

begin
  cmd = "lldpctl -f keyvalue"
  status, stdout, stderr = run_command(:command => cmd)

  stdout.split("\n").each do |element|
    key, value = element.split(/=/)
    elements = key.split(/\./)[1..-1].push value

    hashify lldp, elements
  end 

  lldp
rescue => e
  Chef::Log.warn "Ohai llpd plugin failed with: '#{e}'"
end
